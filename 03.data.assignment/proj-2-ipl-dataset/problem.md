# IPL data set

## Aim

To convert raw open data into charts that tell some kind of story

## Preparation

#### raw data

The data for this exercise is sourced from https://www.kaggle.com/manasgarg/ipl/version/5.

## Instructions

1. Download all the data needed. Consult your mentor if you have any problems
   accessiong the raw data.
2. Initialize a node type script project. All your code should be in typescrpt

### Part 1 :: CSV -> JSON

**Important** This is where all your logic will be. Code a node program which
will load the raw csv and convert into a format that can be used to create plots
in part 2.

### Part 2 :: Plot with high charts 

High charts is an open source lib to create plots on the browser. Use the 
json generated previously to plot. Use ajax to load the data onto the 
browser.

## Problems

### 1. Total runs scored by team

Plot a chart of the total runs scored by each teams over the history of IPL.
Hint: use the total_runs field.

### 2. Top batsman for Royal Challengers Bangalore

Consider only games played by Royal Challengers Bangalore. Now plot the total
runs scored by every batsman playing for Royal Challengers Bangalore over the
history of IPL.

### 3. Foreign umpire analysis

Obtain a source for country of origin of umpires. 
Plot a chart of number of umpires by in IPL by country. Indian umpires should
be ignored as this would dominate the graph.

### 4. Stacked chart of matches played by team by season

Plot a stacked bar chart of ...

- number of games played
- by team 
- by season