# Checklist TypeScript data project


## Create npm project

1. Init npm package
2. create .gitignore and add the folder to be ignore to it.
3. Create and push to a gitlab repo

## Create directories 

1. Create `source_data` directory and download csv into it. Add it to .gitignore
2. Create a `scripts` directory.
3. Create a `frontend` directory.

## Make `scripts` and `frontend` typescript projects
2. Init typescript package
3. Init tslint
4. For the frontend you might need a build directory, which you should add to .gitignore where the generated .js files are kept.
5. Write tranformation scripts
6. Write html + frontend code. The resulting html and .js should hosted on gitlab.io pages.

## Finally
1. Write the README with instructions to build, and run your project


