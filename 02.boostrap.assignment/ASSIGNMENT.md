# Bootstrap 4 Assignment

## First steps

1. Think about a topic you want your website to be about. It can a music / auto fan site, company site, e - commerce website etc.
2. Discuss the topic with your mentor.
3. Collect all digital assets. Images etc.

4. Implement a modern website using bootstrap 4


## Direction

1. The site should have header.
2. It should have a top menu. (can be a part of the header)
3. The site should have a hero unit.
4. The site should have at least 5 sections
5. It should have a footer.

You can use https://mobirise.com/bootstrap-4-theme/ as examples.

